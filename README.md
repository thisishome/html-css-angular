# Codetest #

![Line Graph](https://bitbucket.org/repo/rkpdAj/images/465719911-Bildschirmfoto%202016-04-14%20um%2010.06.02.png)

### What is this repository for? ###

Some small tasks to show off your HTML/CSS (and Angular) skills.

If you are not too familiar with angularjs - have a look at the alternative task at the bottom (no angular needed there).

### How to set it up? ###

This Repo is basically a copy of John Papa's [Modular Example](https://github.com/johnpapa/ng-demos/tree/master/modular).
We strictly follow this [styleguide](https://github.com/johnpapa/angular-styleguide/blob/master/a1/README.md).

* Clone this repo, then install ```npm install && bower install``` (Note: this might take a while)

* Start the server with ```gulp serve-dev```

* You find the modular example here ```http://localhost:7203``` 

### Task ###

In the top picture you see an example of a line graph which will be included in our app soon. We need a way to generate such graphs.

### 1 ###
Create a new controller under ```http://localhost:7203/#/linegraph``` which is displayed in the center of the screen.

Note: If you are not too familiar with John Papa's styleguide, you can also use the (already existing) ```http://localhost:7203/#/avengers``` route+controller and put your solution in there.

### 2 ###
Use the following json inside the controller to display a line graph. 

```vm.data = [{x: 1, y: 50}, {x: 2, y: 80}, {x: 3, y: 20}, {x: 4, y: 100}];```
  
*Note* It is totally fine to use an appropriate library to display the graph.

### 3 ###

Try to implement the style from the picture above (only the line graph - not the box and the surroundings.)

### 4 ###

Once you finished, send an email with your solution back to us. (Please do not include npm and bower files)


## Alternative Task - For the guys who are not familiar with angularjs ##

In the top picture you see an example of a line graph which will be included in our app soon. We need a way to generate such graphs.

### 1 ###
Do *not* clone this repo.

### 2 ###
Design the line-graph from the above image in html/css

### 3 ###
Read the data from a json

```
var data = [{x: 1, y: 50}, {x: 2, y: 80}, {x: 3, y: 20}, {x: 4, y: 100}];
```

### 4 ###
Add an animation which can be shown once the data is changed/loaded.

Hint: You are supposed to use external libraries if needed.

### 5 ###

Once you finished, send an email with your solution (.zip) back to us. (Please do not include npm and bower files)